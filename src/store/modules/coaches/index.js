import mutations from './mutations.js';
import actions from './actions.js';
import getters from './getters.js';

export default {
    namespaced: true,
    state() {
        return {
            coaches: [
                {
                    id: 'c1',
                    firstName: 'Tibo',
                    lastName: 'In Shape',
                    areas: ['fitness', 'street workout', 'nutrition'],
                    description:
                        "Damn, je serais ton coach personnel pour te faire monter au niveau professionnel",
                    hourlyRate: 35
                },
                {
                    id: 'c2',
                    firstName: 'Eric',
                    lastName: 'Flag',
                    areas: ['fitness', 'street workout'],
                    description:
                        'Spécialiste des figures en street workout',
                    hourlyRate: 32
                },
                {
                    id: 'c3',
                    firstName: 'Salim',
                    lastName: 'Sahili',
                    areas: ['fitness', 'nutrition'],
                    description:
                        'Deviens meilleur',
                    hourlyRate: 32
                },
            ]
        }
    },
    mutations: mutations,
    actions: actions,
    getters: getters,
};